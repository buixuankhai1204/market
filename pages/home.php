<?php
include 'admin/product/model/indexModel.php';
include 'admin/category/product/model/indexModel.php';
$product = new product(0, 0, "", "", "", "", 0, 0);
$category = new category(0, 0, "");
$current_page = ($_GET['page'] != 1) ? 1 : $_GET['page'];
$listProduct = $product->getAllProduct($current_page);
$caca = $category->getAllCategory();
$cc = getDatatree($caca['data'], 0);

?>

<div id="main-content-wp" class="home-page clearfix">
    <div class="wp-inner">
        <div class="main-content fl-right">
            <div class="section" id="slider-wp">
                <div class="section-detail">
                    <div class="item">
                        <img src="public/images/slider-01.png" alt="">
                    </div>
                    <div class="item">
                        <img src="public/images/slider-02.png" alt="">
                    </div>
                    <div class="item">
                        <img src="public/images/slider-03.png" alt="">
                    </div>
                </div>
            </div>
            <div class="section" id="support-wp">
                <div class="section-detail">
                    <ul class="list-item clearfix">
                        <li>
                            <div class="thumb">
                                <img src="public/images/icon-1.png">
                            </div>
                            <h3 class="title">Miễn phí vận chuyển</h3>
                            <p class="desc">Tới tận tay khách hàng</p>
                        </li>
                        <li>
                            <div class="thumb">
                                <img src="public/images/icon-2.png">
                            </div>
                            <h3 class="title">Tư vấn 24/7</h3>
                            <p class="desc">1900.9999</p>
                        </li>
                        <li>
                            <div class="thumb">
                                <img src="public/images/icon-3.png">
                            </div>
                            <h3 class="title">Tiết kiệm hơn</h3>
                            <p class="desc">Với nhiều ưu đãi cực lớn</p>
                        </li>
                        <li>
                            <div class="thumb">
                                <img src="public/images/icon-4.png">
                            </div>
                            <h3 class="title">Thanh toán nhanh</h3>
                            <p class="desc">Hỗ trợ nhiều hình thức</p>
                        </li>
                        <li>
                            <div class="thumb">
                                <img src="public/images/icon-5.png">
                            </div>
                            <h3 class="title">Đặt hàng online</h3>
                            <p class="desc">Thao tác đơn giản</p>
                        </li>
                    </ul>
                </div>
                
            <div class="section" id="list-product-wp">
                <div class="section-head">
                    <h3 class="section-title">Điện thoại</h3>
                </div>
                <div class="section-detail">
                    <ul class="list-item clearfix">
                        <?php foreach ($listProduct->data as $item) { ?>
                            <li>
                                <a href="?page=detail_product" title="" class="thumb">
                                    <img width="200px" height="200px" src="<?php echo $config['baseUrl'] ?>/admin/public/image/<?php echo $item->image ?>" id="<?php echo "image" . $item->productId ?>">
                                </a>
                                <a href="?page=detail_product&productId=<?php echo $item->productId ?>" title="" id="<?php echo "name" . $item->productId ?>" class="product-name"><?php echo $item->productName ?></a>
                                <div class="price">
                                    <span class="new" id="<?php echo "price" . $item->productId ?>"><?php echo $item->price ?></span>
                                    <span class="old"><?php echo $item->price * 120 / 100 ?></span>
                                </div>
                                <div class="action clearfix">
                                    <button id="<?php echo $item->productId ?>" title="" class="add-cart fl-left order">Thêm giỏ hàng</button>
                                    <a href="?page=checkout" id="<?php echo $item->productId ?>" title="" class="buy-now fl-right buy-now">Mua ngay</a>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">Trước</span>
                                <span class="sr-only">Sau</span>
                            </a>
                            <?php
                            if ($current_page > 1 && $_SESSION['total_page_product'] > 1) {
                                echo $current_page;
                                echo '<li class="page-item"><a class="page-link" href="?view=list-product&page=' . ($current_page - 1) . '">Prev</a><li class="page-item">';
                            }

                            // Lặp khoảng giữa
                            for ($i = 1; $i <= $_SESSION['total_page_product']; $i++) {
                                // Nếu là trang hiện tại thì hiển thị thẻ span
                                // ngược lại hiển thị thẻ a
                                if ($i == $current_page) {
                                    echo '<span>' . $i . '</span>  ';
                                } else {
                                    echo ' <li class="page-item"><a class="page-link"<a href="?view=list-product&page=' . $i . '">' . $i . '</a> <li class="page-item"> ';
                                }
                            }

                            if ($current_page < $_SESSION['total_page_product'] && $_SESSION['total_page_product'] > 1) {
                                echo '<li class="page-item"><a class="page-link" <a href="?view=list-product&page=' . ($current_page + 1) . '">Next</a> <li class="page-item"> ';
                            }
                            ?>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="sidebar fl-left">
            <div class="section" id="category-product-wp">
                <div class="section-head">
                    <h3 class="section-title">Danh mục sản phẩm</h3>
                </div>
                <div class="secion-detail">
                <ul class="list-item">
                        <?php foreach ($cc as $item) {
                            if ($item['level'] == 0) { ?>
                                <li>

                                    <a href="?page=category_product" title=""><?php if ($item['level'] == 0) {
                                                                                    echo $item['categoryName'];
                                                                                } else {
                                                                                } ?></a>
                                    <ul class="sub-menu">
                                        <?php foreach ($cc as $item1) {
                                            if ($item1['level'] != 0) { ?>

                                                <li>
                                                    <a href="?page=category_product&categoryId=<?php echo $item1['categoryId'] ?>" title=""><?php if ($item1['parentId'] == $item['categoryId']) {
                                                                                                                                                echo str_repeat('', $item1['level']) . $item1['categoryName'];
                                                                                                                                            } ?></a>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>

                                    </ul>
                                </li>

                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="section" id="selling-wp">
                <div class="section-head">
                    <h3 class="section-title">Sản phẩm bán chạy</h3>
                </div>
                <div class="section-detail">
                    <ul class="list-item">
                        <li class="clearfix">
                            <a href="?page=detail_product" title="" class="thumb fl-left">
                                <img src="public/images/img-pro-13.png" alt="">
                            </a>
                            <div class="info fl-right">
                                <a href="?page=detail_product" title="" class="product-name">Laptop Asus A540UP I5</a>
                                <div class="price">
                                    <span class="new">5.190.000đ</span>
                                    <span class="old">7.190.000đ</span>
                                </div>
                                <a href="" title="" class="buy-now">Mua ngay</a>
                            </div>
                        </li>
                        <li class="clearfix">
                            <a href="?page=detail_product" title="" class="thumb fl-left">
                                <img src="public/images/img-pro-11.png" alt="">
                            </a>
                            <div class="info fl-right">
                                <a href="?page=detail_product" title="" class="product-name">Iphone X Plus</a>
                                <div class="price">
                                    <span class="new">15.190.000đ</span>
                                    <span class="old">17.190.000đ</span>
                                </div>
                                <a href="" title="" class="buy-now">Mua ngay</a>
                            </div>
                        </li>
                        <li class="clearfix">
                            <a href="?page=detail_product" title="" class="thumb fl-left">
                                <img src="public/images/img-pro-12.png" alt="">
                            </a>
                            <div class="info fl-right">
                                <a href="?page=detail_product" title="" class="product-name">Iphone X Plus</a>
                                <div class="price">
                                    <span class="new">15.190.000đ</span>
                                    <span class="old">17.190.000đ</span>
                                </div>
                                <a href="" title="" class="buy-now">Mua ngay</a>
                            </div>
                        </li>
                        <li class="clearfix">
                            <a href="?page=detail_product" title="" class="thumb fl-left">
                                <img src="public/images/img-pro-05.png" alt="">
                            </a>
                            <div class="info fl-right">
                                <a href="?page=detail_product" title="" class="product-name">Iphone X Plus</a>
                                <div class="price">
                                    <span class="new">15.190.000đ</span>
                                    <span class="old">17.190.000đ</span>
                                </div>
                                <a href="" title="" class="buy-now">Mua ngay</a>
                            </div>
                        </li>
                        <li class="clearfix">
                            <a href="?page=detail_product" title="" class="thumb fl-left">
                                <img src="public/images/img-pro-22.png" alt="">
                            </a>
                            <div class="info fl-right">
                                <a href="?page=detail_product" title="" class="product-name">Iphone X Plus</a>
                                <div class="price">
                                    <span class="new">15.190.000đ</span>
                                    <span class="old">17.190.000đ</span>
                                </div>
                                <a href="" title="" class="buy-now">Mua ngay</a>
                            </div>
                        </li>
                        <li class="clearfix">
                            <a href="?page=detail_product" title="" class="thumb fl-left">
                                <img src="public/images/img-pro-23.png" alt="">
                            </a>
                            <div class="info fl-right">
                                <a href="?page=detail_product" title="" class="product-name">Iphone X Plus</a>
                                <div class="price">
                                    <span class="new">15.190.000đ</span>
                                    <span class="old">17.190.000đ</span>
                                </div>
                                <a href="" title="" class="buy-now">Mua ngay</a>
                            </div>
                        </li>
                        <li class="clearfix">
                            <a href="?page=detail_product" title="" class="thumb fl-left">
                                <img src="public/images/img-pro-18.png" alt="">
                            </a>
                            <div class="info fl-right">
                                <a href="?page=detail_product" title="" class="product-name">Iphone X Plus</a>
                                <div class="price">
                                    <span class="new">15.190.000đ</span>
                                    <span class="old">17.190.000đ</span>
                                </div>
                                <a href="" title="" class="buy-now">Mua ngay</a>
                            </div>
                        </li>
                        <li class="clearfix">
                            <a href="?page=detail_product" title="" class="thumb fl-left">
                                <img src="public/images/img-pro-15.png" alt="">
                            </a>
                            <div class="info fl-right">
                                <a href="?page=detail_product" title="" class="product-name">Iphone X Plus</a>
                                <div class="price">
                                    <span class="new">15.190.000đ</span>
                                    <span class="old">17.190.000đ</span>
                                </div>
                                <a href="" title="" class="buy-now">Mua ngay</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="section" id="banner-wp">
                <div class="section-detail">
                    <a href="" title="" class="thumb">
                        <img src="public/images/banner.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>